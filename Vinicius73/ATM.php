<?php namespace Vinicius73;

class ATM
{
    static protected $cells = [100, 50, 20, 10];

    /**
     * @param $value
     *
     * @return array
     * @throws InvalidArgumentException
     * @throws NoteUnavailableException
     */
    public function withdraw($value)
    {
        $value = $this->validate($value);

        if ($value):
            return $this->calculateCell($value);
        else:
            return [];
        endif;
    }

    /**
     * @param $value
     *
     * @return bool|int
     * @throws InvalidArgumentException
     * @throws NoteUnavailableException
     */
    protected function validate($value)
    {
        if (empty($value)) return false;

        if (is_integer($value)):
            if ($value <= 0):
                throw new InvalidArgumentException('Invalid value.');
            elseif (($value % 10) != 0):
                throw new NoteUnavailableException('It is not possible to withdraw the requested amount.');
            endif;
        else:
            return false;
        endif;

        return (int)$value;
    }

    /**
     * @param $value
     *
     * @return array
     */
    protected function calculateCell($value)
    {
        $cells = [];

        foreach ($this->getAvailableCell() as $cell):

            if ($value < $cell) continue;
            //
            $qtd          = floor(($value / $cell));
            $cells[$cell] = $qtd;
            $cellsAmount  = $cell * $qtd;

            $value -= $cellsAmount;

            if ($value <= 0) break;
        endforeach;

        return $cells;
    }

    /**
     * @return array
     */
    protected function getAvailableCell()
    {
        return self::$cells;
    }
}