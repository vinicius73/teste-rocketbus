# Teste / RocketBus

## Cash Machine
- Referência [https://github.com/RocketBus/developers/](https://github.com/RocketBus/developers/tree/master/want-to-be-clickbus/cash-machine)
- Foi usado o [Slim Framework](http://www.slimframework.com/) apenas para a camada http e [composer](http://getcomposer.org) para gerenciar as
dependências
- Testes unitários feitos com [PHPUnit](https://phpunit.de/)
- Uma mini aplicação feita com [AngularJS](https://angularjs.org/) para uma fácil utilização

### Instalação
``composer install`` ou ``php composer.phar install``

### Uso
Para o uso via http o servidor deve ser direcionado a ``./public`` com reestrita de url para ``index.php``

### Testes
``php ./vendor/bin/phpunit``

-----------
 Por [Vinicius73](https://github.com/vinicius73) em novembro/2014