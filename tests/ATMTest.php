<?php
use Vinicius73\ATM;

class ATMTest extends PHPUnit_Framework_TestCase
{
    private $name = 'Teste ATM';
    /**
     * @var ATM
     */
    private $ATM;

    public function setUp()
    {
        $this->ATM = new ATM();
    }

    public function testFirst()
    {
        $cells = $this->ATM->withdraw(30);

        $this->assertEquals([20 => 1, 10 => 1], $cells);
    }

    public function testSecond()
    {
        $cells = $this->ATM->withdraw(80);

        $this->assertEquals([50 => 1, 20 => 1, 10 => 1], $cells);
    }

    public function testThird()
    {
        try {
            $cells = $this->ATM->withdraw(125);
        } catch (\Vinicius73\NoteUnavailableException $InvalidArgumentException) {
            return;
        };

        $this->fail('Expected Exception \Vinicius73\NoteUnavailableException');
    }

    public function testFourth()
    {
        try {
            $cells = $this->ATM->withdraw(-130);
        } catch (\Vinicius73\InvalidArgumentException $InvalidArgumentException) {
            return;
        };

        $this->fail('Expected Exception \Vinicius73\InvalidArgumentException');
    }

    public function testFifth()
    {
        $cells  = $this->ATM->withdraw(null);
        $cells2 = $this->ATM->withdraw('');

        $this->assertEmpty($cells, 'NULL param');
        $this->assertEmpty($cells2, 'empty string');

    }
}
 