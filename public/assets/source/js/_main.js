(function () {
    app = angular.module('app', []);

    app.controller('MainController', ["$scope", "$http", function ($scope, $http) {
        var withdraw = function () {
            var value = $scope.value;

            $scope.load = true;

            $http({
                url: '/withdraw',
                method: "GET",
                params: {value: value}
            }).success(function (data, status, headers, config) {
                $scope.load = false;
                $scope.cells = data;
                $scope.msg = false;
            }).error(function (data, status, headers, config) {
                $scope.load = false;
                $scope.cells = [];
                $scope.msg = 'ERRO! :-(';
            });
        };
        $scope.value = 230;
        $scope.load = false;
        $scope.msg = false;
        $scope.cells = {};
        $scope.withdraw = withdraw;
    }]);

}).call(this);