<?php
require 'vendor/autoload.php';

define('APP_DIR', __DIR__);
define('APP_VIEWS', APP_DIR . '/views');

use Vinicius73\ATM;

$app = new \Slim\Slim();

$app->view->setTemplatesDirectory(APP_VIEWS);

$app->get(
    '/', function () use ($app) {
        $app->render('home.php');
    }
);

$app->map(
    '/withdraw', function () use ($app) {
        $value = (int) $app->request->params('value');
        $ATM   = new ATM();
        $cells = $ATM->withdraw($value);
        echo json_encode($cells);
    }
)->via('GET', 'POST')->name('withdraw');;


$app->run();