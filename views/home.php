<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>ATM 73</title>

    <link href="assets/pub/css/main.css" rel="stylesheet">

    <script src="assets/pub/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>

<!-- Wrap all page content here -->
<div id="wrap" ng-app="app">

    <!-- Begin page content -->
    <div class="container">
        <div class="page-header">
            <h1>Bem-Vindo ao ATM 73</h1>
        </div>
        <div class="col-md-7">
            <div class="panel panel-default" ng-controller="MainController">
                <div class="panel-body">
                    <p class="lead">Por favor, informe o valor que você deseja retirar!</p>
                    <div class="input-group">
                        <input ng-model="value" type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" ng-click="withdraw()" type="button">Sacar!</button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <hr/>
                    <p class="alert alert-warning" ng-hide="!msg">{{msg}}</p>
                    <div class="progress" ng-hide="!load">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            <span class="sr-only">Carregando...</span>
                        </div>
                    </div>

                    <ul class="list-group">
                        <li class="list-group-item" ng-repeat="(cell, qtd) in cells">
                            {{qtd}} nota(s) de {{cell}}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="footer">
    <div class="container">
        <p class="text-muted">
            Por <a href="https://github.com/vinicius73">Vinicius Reis</a>
        </p>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.3/angular.min.js"></script>
<script src="assets/pub/js/vendor/jquery-2.1.1.min.js"></script>
<script src="assets/pub/js/vendor/jquery-migrate-1.2.1.min.js"></script>
<script src="assets/pub/js/app.js"></script>
</body>
</html>